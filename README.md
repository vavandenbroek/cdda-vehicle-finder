# CDDA Vehicle finder

Lost your car again? Forgot to use "Remember vehicle location"? Don't want to have to go through every building in the entire city again? Fear not! This script scans your savegame, and tells you exactly where your vehicle is.

## Requirements

*  Python 3

## Usage

1.  Put FindVehicle.py in your save folder ( CDDA/save/Worldname/ ). Feel free to make a copy of it first, but this really shouldn't be writing anything.
2.  Run FindVehicle.py
3.  Select items or vehicles. For vehicles:
4.  Type the name or type of the vehicle or item that you want to see more of (must be an exact match)
5.  For vehicles: Using the vehicle inventories, figure out which one is yours
6.  Go to te map coordinates indicated by the script, for example "110.88.0"
7.  Find your vehicle, give it a hug, and enable "Remember vehicle location" so this doesn't happen again. For items, just pick it up.


Of course this can also be used to find interesting vehicles in your map, if you want to.

NOTE: Vehicles you built yourself typically have 'none' as type. 
