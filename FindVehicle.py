import json
from os import walk

# Returns all vehicles in a .map file, in JSON
def ReadMapFileVehicle(mapfilename):
    VehicleList = []
    with open(mapfilename, 'r') as file:
        for i in json.loads(file.read()):
          vehicle = i['vehicles']
          if vehicle:
              VehicleList.extend(vehicle)
        return VehicleList

# Returns all items in a .map file, in JSON
def ReadMapFileItems(mapfilename):
    itemnamelist = []
    with open(mapfilename, 'r') as file:
        for i in json.loads(file.read()):
            itemsinmap = i['items']
            #print(itemsinmap)   #looks like [number, number, [item, item, item]]
            if itemsinmap:
                for item in itemsinmap[2]:
                    if isinstance(item, list): #some items are weird like this. We just skip them for now.
                        for subitem in item:
                            if not isinstance(subitem, str):
                                itemnamelist.append(item)
                    else:
                        itemnamelist.append(item)
        return itemnamelist

# Removes the .map at the end of the filename
def NameToCoords(filename):
    return filename[0:-4]

# Returns a list of tuples like this:
# [(coordinates, vehicle)]
# That contains every vehicle in mapfoldername
def ListFolderVehicles(mapfoldername):
    (_, _, filelist) = next(walk(mapfoldername))
    vehiclelist = []
    tuplelist = []
    filename = ''
    for file in filelist:
        if file is not '':
            filename = mapfoldername + '/' + file
            vehiclelist = (ReadMapFileVehicle(filename))
            for vehicle in vehiclelist:
                tuplelist.append((vehicle, NameToCoords(file)))
    return tuplelist

# Returns a dictionary of (coords, [item]) of all coordinates where an item can be found.
def ListFolderItems(mapfoldername):
    (_, _, filelist) = next(walk(mapfoldername))
    itemlist = []
    tuplelist = {}
    filename = ''
    coords = ''
    for file in filelist:
        if file is not '':
            filename = mapfoldername + '/' + file
            itemlist = (ReadMapFileItems(filename))
            coords = NameToCoords(file)
            # print(itemlist)
            for item in itemlist:
                if not coords in tuplelist:
                    tuplelist[coords] = []
                tuplelist[coords].append(item)
    return tuplelist

# Returns all vehicles in the save, in JSON
# returns [vehicle]
def ListAllFolderVehicles():
    (_, folderlist, _) = next(walk('./maps'))
    vehiclelist = []
    foldername = ''
    for folder in folderlist:
        foldername = './maps/' + folder 
        vehiclelist.extend(ListFolderVehicles(foldername))
    return vehiclelist

# Returns all items in the save, in JSON
# returns {coords: [item]}
def ListAllFolderItems():
    (_, folderlist, _) = next(walk('./maps'))
    itemlist = {}
    foldername = ''
    for folder in folderlist:
        foldername = './maps/' + folder 
        itemlist.update(ListFolderItems(foldername))
    return itemlist

# Returns all items in a vehicle
def VehicleItems(vehicle):
    itemlist = []
    for part in vehicle['parts']:
        if part['items']:
            for item in part['items']:
                itemlist.append(item['typeid'])
    return itemlist

# # Returns all items in an item
# def ItemItems(item):
#     itemlist = []
#     if item['contents']:
#         for item in part['items']:
#             itemlist.append(item['typeid'])
#     return itemlist
# Takes a list like [1, 9, {someItem}, 0, 2, {someItem2}]
# And turns it into [(someItem)]
# def valuesToItemCoords


def ProgramEntry():
    name = ''
    while True:
        vehicleorItem = input('Search items (0) or vehicles (1)?\n')
        if vehicleorItem == '0':
            coorddictionary = ListAllFolderItems()
            itemlist = []
            # Sort the items so we can print a single entry for each type
            for key, value in coorddictionary.items():
                for item in value:
                    if isinstance(item, list):
                        # print(item)
                        for subitem in item:
                            if not isinstance(subitem, int) and not isinstance(subitem, str):
                                # print(subitem)
                                if subitem['typeid'] not in itemlist:
                                    itemlist.append(subitem['typeid'])
                    elif item['typeid'] not in itemlist:
                        itemlist.append(item['typeid'])
            # Print the item types
            itemlist.sort()
            for i in itemlist:
                print(i)
            print('')
            expandtype = input('Which Item type would you like to expand? (Case sensitive!)\n')
            print('')
            for key, value in coorddictionary.items():
                for x in range(len(value)):
                    if not isinstance(value[x], int):   #don't check if its a tile coordinate
                        if not isinstance(value[x], list):  
                            if value[x]['typeid'] == expandtype:
                                print("Map coordinates: {}".format(key))
                                print(value[x])

        else:
            tuplelist = ListAllFolderVehicles()
            typeorname = input('Sort by type (0) or by name (1)?\n')
            if typeorname == '0':
                tuplelist.sort(key=lambda tup: tup[0]['type'])
                for (vehicle, _) in tuplelist:
                    if vehicle['type'] != name:
                        name = vehicle['type']
                        print(name)
            else:
                tuplelist.sort(key=lambda tup: tup[0]['name'])
                for (vehicle, _) in tuplelist:
                    if name != vehicle['name']:
                        name = vehicle['name']
                        print(name)
            print('')
            expandtype = input('Which Vehicle would you like to expand? (Case sensitive!)\n')
            print('')
            if typeorname is '0':
                print('type')
                for (vehicle, coord) in tuplelist:
                    if vehicle['type'] == expandtype:
                        print('%s, %s:' % (vehicle['type'], coord))
                        print(VehicleItems(vehicle))
            else:
                for (vehicle, coord) in tuplelist:
                    if vehicle['name'] == expandtype:
                        print('%s, %s:' % (vehicle['name'], coord))
                        print(VehicleItems(vehicle))
        print('Search completed! Starting new search...\n')

ProgramEntry()
